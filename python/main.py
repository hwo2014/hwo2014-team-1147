import json
import socket
import sys
import math
import sys # Just for sys.stderr.write

from operator import itemgetter

QUALIFYING = True
DEBUG = not QUALIFYING
FOLLOWING_TARGET_SPEEDS = True

EPSILON = 25 # Degrees
SLIP_FACTOR = 0.5

VERY_NEAR = 25
NEAR = 38

TARGET_SPEED_INIT = 5.5
VERY_FAST = 7
FAST = 6
SLOW = 5

FcFactor = 0.6

class NoobBot(object):
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.color = ""
        self.key = key

        self.tick = -1
        self.last_tick = -1

        self.last_in_piece_distance = 0
        self.last_piece_index = -1

        self.track = []
        self.lanes = {} # lanes[x] is the order (from left to right) of lane
                        # with "index" x on the track.
        self.lane = 0   # Current lane index.

        self.speed = 0          # distance/ticks
        self.target_speeds = [] # Tagert speed of each trach segment (piece)

        self.longest_straight_index = None
        self.longest_straight_length = None
        self.longest_straight_end = None

        self.last_switch_index = -1

        self.turbo_available = False

        self.testing_vmax = True
        self.Fc = 0 # Maximum centrifugal force without crash
        self.last_speed = 0

    def msg(self, msg_type, data):
        if (self.tick != self.last_tick):
            message = {"msgType": msg_type, "data": data, "gameTick": self.tick}
        else:
            message = {"msgType": msg_type, "data": data}
        self.send(json.dumps(message))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("joinRace", {
            "botId": {
                "name": self.name,
                "key": self.key
            },
            "trackName": "keimola",
            #"trackName": "germany",
            #"trackName": "usa",
            #"trackName": "france",
            "carCount": 1
        })

    def join_qualifying(self):
        return self.msg("join", {
            "name": self.name,
            "key": self.key
        })

    def throttle(self, throttle):
        if DEBUG:
            print('THROTTLE: ' + str(throttle))
        self.msg("throttle", throttle)

    def is_turbo_time(self, index):
        if self.turbo_available and index == self.longest_straight_index:
            return True
        return False

    def apply_turbo(self, quote):
        if DEBUG:
            print('TURBO!')
        self.turbo_available = False
        self.msg("turbo", quote)

    def switch(self, direction):
        if DEBUG:
            print('SWITCH: ' + direction)
        self.msg("switchLane", direction)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        if QUALIFYING:
            self.join_qualifying()
        else:
            self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        #self.ping()

    # Get from server the confirmation of car's name and color.
    def on_your_car(self, data):
        self.name = data['name']
        self.color= data['color']
        print("My name: " + self.name)
        print("My color: " + self.color)

    def on_game_init(self, data):
        # Get track pieces from server.
        self.track = data['race']['track']['pieces']

        # Initialize target speeds.
        if self.target_speeds == []:
            self.target_speeds = [TARGET_SPEED_INIT] * len(self.track)

        # Initialize lanes dictionary
        lane_dicts = data['race']['track']['lanes']
        lane_dicts.sort(key=itemgetter('distanceFromCenter'))
        c = 0
        for lane in lane_dicts:
            self.lanes[lane['index']] = {'index': c,
                                         'offset': lane['distanceFromCenter']}
            c += 1

        # Find longest straight.
        self.longest_straight_index, \
        self.longest_straight_length, \
        self.longest_straight_end = self.get_longest_straight()
        if DEBUG:
            print('\n>>> Longest straight index: ' +
                  str(self.longest_straight_index) +
                  '; length: ' + str(self.longest_straight_length))

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    # Drive the car.
    def on_car_positions(self, data):
        pedal = 1
        slip = 0
        for d in data:
            if d['id']['color'] == self.color:
                piece = d['piecePosition']
                self.update_speed(piece)
                self.update_lane(piece)

                if self.testing_vmax:
                    self.testing_behavior(piece['pieceIndex'])
                    return

                if self.will_switch(piece):
                    return

                if self.is_turbo_time(piece['pieceIndex']):
                    self.apply_turbo('Hoje nao! Hoje nao!')
                    return

                next_piece_angle = self.get_piece_angle(piece['pieceIndex'] + 1)
                distance = self.get_distance_to_piece_end(piece)

                if FOLLOWING_TARGET_SPEEDS:
                    pedal = self.match_speed(piece['pieceIndex'])

                elif next_piece_angle >= 30:
                    if self.speed > VERY_FAST:
                        if distance < VERY_NEAR:
                            pedal = 0
                        elif distance < NEAR:
                            pedal = 0
                        else:
                            pedal = 0.3
                    elif self.speed > FAST:
                        if distance < VERY_NEAR:
                            pedal = 0.2
                        elif distance < NEAR:
                            pedal = 0.4
                        else:
                            pedal = 0.7

                #if self.speed > SLOW and next_piece_angle > EPSILON:
                #    slip = min(math.fabs(d['angle']), EPSILON) / EPSILON
                #    if DEBUG:
                #        print('SLIPPING Original pedal: ' + str(pedal) + \
                #              '; slip: ' + str(slip))

                self.throttle(max(0, pedal - SLIP_FACTOR * slip))
                return

    def testing_behavior(self, index):
        if 'angle' in self.track[index]:
            self.throttle(1.0)
        else:
            self.throttle(self.match_speed(index))

    def on_turbo_available(self, data):
        print("Turbo available")
        self.turbo_available = True

    def on_crash(self, data):
        print("Someone crashed")
        if self.color == data['color']:
            if 'radius' in self.track[self.last_piece_index]:
                Fc = FcFactor * (self.last_speed * self.last_speed /
                          self.track[self.last_piece_index]['radius'])
                if self.Fc == 0 or Fc < self.Fc:
                    self.Fc = Fc
                self.update_target_speeds()
            sys.stderr.write("Car crashed; V_crash: " + str(self.last_speed) +
                             "; Fc: " + str(self.Fc) + "\n")
        #self.ping()

    def on_game_end(self, data):
        print("Race ended")
        #self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        #self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'turboAvailable': self.on_turbo_available,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            if DEBUG:
                print('\n' + line),
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                self.updateTickCounter(msg)
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                #self.ping()
            line = socket_file.readline()

    def updateTickCounter(self, msg):
        if 'gameTick' in msg:
            self.last_tick = self.tick
            self.tick = msg['gameTick']

    def update_speed(self, piece):
        self.last_speed = self.speed

        index = piece['pieceIndex']
        if index == self.last_piece_index:
            self.speed = piece['inPieceDistance'] - self.last_in_piece_distance
        else:
            self.speed = piece['inPieceDistance'] + \
                         (self.get_piece_length(index - 1) - \
                          self.last_in_piece_distance)
            self.last_piece_index = index
        self.last_in_piece_distance = piece['inPieceDistance']
        if DEBUG:
            print("SPEED: " + str(self.speed))

    def update_lane(self, piece):
        self.lane = piece['lane']['endLaneIndex']

    def get_piece_length(self, index):
        index = index % len(self.track)

        if 'length' in self.track[index]:
            # It is a straight segment
            return self.track[index]['length']
        else:
            # It is a bend
            R = self.track[index]['radius'] - self.lanes[self.lane]['offset']
            if DEBUG:
                print("Radius: " + str(R))
            return 2 * math.pi * R * math.fabs(self.track[index]['angle']) / 360

    def get_piece_angle(self, index):
        if index >= len(self.track):
            index = index % len(self.track)

        if 'length' in self.track[index]:
            # It is a straight segment
            return 0.0
        else:
            # It is a bend
            return math.fabs(self.track[index]['angle'])

    def get_distance_to_piece_end(self, piece):
        return self.get_piece_length(piece['pieceIndex']) - piece['inPieceDistance']

    def get_next_turn_direction(self, index):
        index += 1
        if index >= len(self.track):
            index = index % len(self.track)
        while 'angle' not in self.track[index]:
            index += 1
            if index >= len(self.track):
                index = index % len(self.track)
        if self.track[index]['angle'] > 0:
            return 'Right'
        else:
            return 'Left'

    # Returns the index of first piece of the longest straight and total length.
    # If there is no straight in the track, returns None, None.
    def get_longest_straight(self):
        index = 0
        end = 0
        length = -1

        i = 0

        # Skip the starting straight.
        while 'length' in self.track[i]:
            i += 1
            if i >= len(self.track):
                return None, None

        first_turn = i
        i += 1
        if i >= len(self.track):
            i = i % len(self.track)
        while i != first_turn:
            # Skip turn.
            while 'angle' in self.track[i]:
                i += 1
                if i >= len(self.track):
                    i = i % len(self.track)

            current_length = 0
            current_index = i
            current_end = i
            while 'length' in self.track[i]:
                current_length += self.track[i]['length']
                current_end = i
                i += 1
                if i >= len(self.track):
                    i = i % len(self.track)

            if current_length > length:
                index = current_index
                end = current_end
                length = current_length

        return index, length, end

    def can_switch_to(self, direction):
        if direction == "Left":
            if self.lanes[self.lane]['index'] == 0:
                return False
        else:
            if self.lanes[self.lane]['index'] == len(self.lanes) - 1:
                return False
        return True

    def will_switch(self, piece):
        index = (piece['pieceIndex'] + 1) % len(self.track)
        if index != self.last_switch_index and 'switch' in self.track[index]:
            direction = self.get_next_turn_direction(index)
            if self.can_switch_to(direction):
                self.switch(direction)
                self.last_switch_index = index
                return True
        return False

    def update_target_speeds(self):
        print('>>> UpdateTargetSpeeds - Fc: ' + str(self.Fc))

        self.testing_vmax = False
        # Update turns
        for i in range(len(self.target_speeds)):
            if 'angle' in self.track[i]:
                self.target_speeds[i] = math.sqrt(self.Fc *
                                                  self.track[i]['radius'])
        # Update straights based on next turn
        for i in range(len(self.target_speeds)):
            next_i = (i + 1) % len(self.target_speeds)
            if 'angle' not in self.track[i] and 'angle' in self.track[next_i]:
                self.target_speeds[i] *= 0.9

        for i in range(len(self.target_speeds) - 2, -1, -1):
            if 'angle' in self.track[i + 1] and 'length' in self.track[i]:
                j = i
                c = 0
                while j >= 0 and 'length' in self.track[j] and c < 1:
                    self.target_speeds[j] = self.target_speeds[i + 1]
                    c += 1
                    j -= 1

        for i in range(len(self.target_speeds)):
            print("TargetSpeed[" + str(i) + "]: " + str(self.target_speeds[i]))

    # Returns throttle needed to match target speed of current track segment.
    def match_speed(self, index):
        pedal = 0.5
        if self.speed > 1.20 * self.target_speeds[index] or \
            (self.target_speeds[index] == 15 and
            self.longest_straight_end - index < 2 and
            self.speed > 1.40 * self.target_speeds[self.longest_straight_end + 1]):

            pedal = 0.0
        elif self.speed > self.target_speeds[index]:
            pedal = 0.33
        elif self.speed < self.target_speeds[index]:
            pedal = 1
        return pedal

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()

# TODO:
#
# Find the maximum speed allowed in each segment according to its angle.
#
# Set throttle value according to NEXT segment max speed.
#
# Create dprint() for a cleaner debug messaging option
#
# Handle special case: start of the race / recover from crash
#
# Beat 7.73s time (the current first place)
#
# Call will_switch when car is over the previous switch piece, i.e. as soon as
# possible. Only the first car to call switch will do it if more than one is on
# switch piece at the same time.
